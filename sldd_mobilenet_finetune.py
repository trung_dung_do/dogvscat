import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import mobilenet
from tensorflow.keras.layers import Dense, BatchNormalization
from tensorflow.keras import Model
from tensorflow.keras.optimizers import Adam
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import itertools
import numpy as np

import os
import shutil
import random


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def prepare_data():
    os.chdir('data/SLDD')
    if not os.path.exists('train'):
        os.mkdir('train')
        os.mkdir('valid')
        os.mkdir('test')
        for i in range(0, 10):
            shutil.move(f'{i}', 'train')
            os.mkdir(f'valid/{i}')
            os.mkdir(f'test/{i}')

            valid_samples = random.sample(os.listdir(f'train/{i}'), 30)
            for j in valid_samples:
                shutil.move(f'train/{i}/{j}', f'valid/{i}')

            test_samples = random.sample(os.listdir(f'train/{i}'), 5)
            for j in test_samples:
                shutil.move(f'train/{i}/{j}', f'test/{i}')
    else:
        print('The data already existed.')
    os.chdir('../..')


prepare_data()
train_path = 'data/SLDD/train'
valid_path = 'data/SLDD/valid'
test_path = 'data/SLDD/test'

dataGen = ImageDataGenerator(preprocessing_function=tf.keras.applications.mobilenet.preprocess_input)

train_batches = dataGen.flow_from_directory(train_path, target_size=(224, 224), batch_size=10)
valid_batches = dataGen.flow_from_directory(valid_path, target_size=(224, 224), batch_size=10)
test_batches = dataGen.flow_from_directory(test_path, target_size=(224, 224), batch_size=10, shuffle=False)

# create a model
mobilenet = mobilenet.MobileNet()
# mobilenet.summary()

x = mobilenet.layers[-6].output
output = Dense(units=10, activation='softmax')(x)
model = Model(inputs=mobilenet.input, outputs=output)



for layer in model.layers[:-23]:
    layer.trainable = False

model.summary()


# compile and train the model
model.compile(optimizer=Adam(0.0001), loss='categorical_crossentropy', metrics=['accuracy'])
model.fit(x=train_batches, steps_per_epoch=len(train_batches),
          validation_data=valid_batches,
          validation_steps=len(valid_batches),
          epochs=30,
          verbose=2)

model.save('sldd-mobilenet.h5')

# test the model
test_labels = test_batches.classes
predictions = model.predict(x=test_batches, steps=len(test_batches), verbose=0)
cm = confusion_matrix(y_true=test_labels, y_pred=predictions.argmax(axis=1))

cm_plot_labels = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
plot_confusion_matrix(cm, classes=cm_plot_labels, title='Confusion Matrix')
plt.show()

print('Done!')



