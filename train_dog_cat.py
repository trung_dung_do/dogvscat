from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, MaxPool2D, Flatten, Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
import seaborn as sn
import pandas as pd

import itertools
import numpy as np
import cv2
import os



train_path = 'data/train/'
validation_path = 'data/validation/'
test_path = 'data/test/'


dataGen = ImageDataGenerator(preprocessing_function=tf.keras.applications.vgg16.preprocess_input)

train_batches = dataGen.flow_from_directory(directory=train_path,
                                            target_size=(224, 224),
                                            batch_size=10,
                                            classes=['dog', 'cat'])

validation_batches = dataGen.flow_from_directory(directory=validation_path,
                                                 target_size=(224, 224),
                                                 batch_size=10,
                                                 classes=['dog', 'cat'])

test_batches = dataGen.flow_from_directory(directory=test_path,
                                           target_size=(224, 224),
                                           batch_size=10,
                                           classes=['dog', 'cat'],
                                           shuffle=False)


def plotImages(images_arr):
    fig, axes = plt.subplots(1, 10, figsize=(20, 20))
    axes = axes.flatten()
    for img, ax in zip(images_arr, axes):
        ax.imshow(img)
        ax.axis('off')
    plt.tight_layout()
    plt.show()


imgs, labels = next(train_batches)

# for i in range(imgs.shape[0]):
#     cv2.imshow('test', imgs[i])
#     cv2.waitKey(0)

print(imgs.shape)
print(labels.shape)

def train_model():
    model = Sequential()
    model.add(Conv2D(filters=32, kernel_size=(3, 3),
                     activation='relu',
                     padding='same',
                     input_shape=(224, 224, 3)))
    model.add(MaxPool2D(pool_size=(2, 2), strides=2))
    model.add(Conv2D(filters=64, kernel_size=(3, 3),
                     activation='relu',
                     padding='same'))
    model.add(MaxPool2D(pool_size=(2, 2), strides=2))
    model.add(Flatten())
    model.add(Dense(units=2, activation='softmax'))

    model.summary()
    # plotImages(imgs)

    model.compile(optimizer=Adam(learning_rate=0.0001),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    model.fit(x=train_batches, validation_data=validation_batches, epochs=10, verbose=2)

    # save the model
    model.save('model-dog-cat.h5')


def plot_confusion_matrix(cm, classes,
                        normalize=False,
                        title='Confusion matrix',
                        cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def inference():
    cwd_path = os.path.curdir
    print(cwd_path)
    my_path = os.path.join(cwd_path, 'model-dog-cat.h5')

    # if my_path.exists():
    if os.path.exists(my_path):
        print('loading model...')
        model = load_model('model-dog-cat.h5')
    else:
        print('training model...')
        train_model()

    predictions = model.predict(x=train_batches, verbose=2)
    predictions = np.argmax(predictions, axis=1)

    cm = confusion_matrix(y_true=train_batches.classes, y_pred=predictions)
    cm_plot_label = ['dog', 'cat']
    plot_confusion_matrix(cm, classes=cm_plot_label, title='Confusion matrix')
    plt.show()


# print(test_batches.class_indices)
# print(test_batches.class_indices)
inference()
print('Done!')

