from tensorflow.keras.applications import mobilenet
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import load_model
import tensorflow as tf
import cv2
from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
import itertools
import os


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


train_path = 'data/train/'
validation_path = 'data/validation/'
test_path = 'data/test/'


dataGen = ImageDataGenerator(
    preprocessing_function=tf.keras.applications.mobilenet.preprocess_input)


train_batches = dataGen.flow_from_directory(directory=train_path,
                                            target_size=(224, 224),
                                            batch_size=10,
                                            classes=['dog', 'cat'])

validation_batches = dataGen.flow_from_directory(directory=validation_path,
                                                 target_size=(224, 224),
                                                 batch_size=10,
                                                 classes=['dog', 'cat'])

test_batches = dataGen.flow_from_directory(directory=test_path,
                                           target_size=(224, 224),
                                           batch_size=10,
                                           classes=['dog', 'cat'],
                                           shuffle=False)


def train_model():
    mobilenet_model = mobilenet.MobileNet()
    mobilenet_model.summary()

    model = Sequential()
    for layer in mobilenet_model.layers[:-5]:
        model.add(layer)

    # model.summary()

    print('type of mobilenet model: ', type(mobilenet_model))
    print('type of our model: ', type(model))

    model.add(Dense(units=2))
    model.add(Activation('softmax'))

    for layer in model.layers[:-23]:
        layer.trainable = False

    model.summary()

    model.compile(optimizer=Adam(learning_rate=0.0001),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    model.fit(x=train_batches, validation_data=validation_batches,
              epochs=20, verbose=2)

    model.save('model-dog-cat-mobilenet.h5')
    return model


def inference():
    model_name = 'model-dog-cat-mobilenet.h5'
    model_path = os.path.join(os.path.curdir, model_name)
    if os.path.exists(model_path):
        model = load_model(model_path)
    else:
        model = train_model()

    predictions = model.predict(
        x=test_batches, steps=len(test_batches), verbose=0)
    predictions = np.argmax(predictions, axis=1)
    cm = confusion_matrix(y_true=test_batches.classes, y_pred=predictions)
    cm_plot_labels = ['dog', 'cat']
    plot_confusion_matrix(cm, cm_plot_labels, title='Confusion Matrix')
    plt.show()


print('length of train batches: ', len(train_batches))
images, labels = next(train_batches)
print('number of images: ', len(images))
print('number of labels: ', len(labels))


def show_images(images, labels):
    for image, label in zip(images, labels):
        if label[0] == 1.:
            st = 'dog'
        else:
            st = 'cat'
        cv2.putText(image, st, (20, 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0))
        cv2.imshow('test image', image)
        cv2.waitKey(0)


inference()
# show_images(images, labels)

print('Done!')
