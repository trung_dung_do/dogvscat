from tensorflow.keras import Sequential, Model, Input
from tensorflow.keras.layers import Conv2D, Flatten, Dense, BatchNormalization, Activation
from tensorflow.keras.layers import concatenate, MaxPool2D, AveragePooling2D, Dropout


def shallownet_sequential(width, height, depth, classes):
    model = Sequential()
    inputShape = (height, width, depth)

    model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same',
                     input_shape=inputShape,
                     activation='relu'))


    model.add(Flatten())
    model.add(Dense(units=classes, activation='softmax'))

    return model


def minigooglenet_functional(width, height, depth, classes):
    def conv_module(x, K, kX, kY, stride, chanDim, padding='same'):
        x = Conv2D(filters=K, kernel_size=(kX, kY), strides=stride, padding=padding)(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = Activation('relu')(x)
        return x

    def inception_module(x, numK1x1, numK3x3, chanDim):
        conv_1x1 = conv_module(x, numK1x1, 1, 1, (1, 1), chanDim)
        conv_3x3 = conv_module(x, numK3x3, 3, 3, (1, 1), chanDim)
        x = concatenate([conv_1x1, conv_3x3], axis=chanDim)
        return x

    def downsample_module(x, K, chanDim):
        conv_3x3 = conv_module(x, K, 3, 3, (2, 2), chanDim, padding='valid')
        pool = MaxPool2D(pool_size=(2, 2), strides=(2, 2))(x)
        return concatenate([conv_3x3, pool], axis=chanDim)

    # define the model input and the first conv module followed
    inputShape = (height, width, depth)
    chanDim= -1
    inputs = Input(shape=inputShape)
    x = conv_module(inputs, 96, 3, 3, (1, 1), chanDim)

    x = inception_module(x, 32, 32, chanDim)
    x = inception_module(x, 32, 48, chanDim)
    x = downsample_module(x, 80, chanDim)

    x = inception_module(x, 112, 48, chanDim)
    x = inception_module(x, 96, 64, chanDim)
    x = inception_module(x, 80, 80)
    x = inception_module(x, 48, 96)
    x = downsample_module(x, 96, chanDim)

    x = inception_module(x, 176, 160, chanDim)
    x = inception_module(x, 176, 160, chanDim)
    x = AveragePooling2D(pool_size=(7, 7))(x)
    x = Dropout(0.5)(x)

    x = Flatten()(x)
    x = Dense(units=classes, activation='softmax')(x)

    model = Model(inputs=inputs, outputs=x, name='minigooglenet')

    return model


class MiniVGGNetModel(Model):
    def __init__(self, classes, chanDim=-1):
        super(MiniVGGNetModel, self).__init__()
        self.conv1A = Conv2D(filters=32, kernel_size=(3, 3), padding='same')
        self.act1A = Activation('relu')
        self.bn1A = BatchNormalization(axis=chanDim)
        self.conv1B = Conv2D(filters=32, kernel_size=(3, 3), padding='same')
        self.act1B = Activation('relu')
        self.bn1B = BatchNormalization(axis=chanDim)
        self.pool1 = MaxPool2D(pool_size=(2, 2))

        self.conv2A = Conv2D(filters=32, kernel_size=(3, 3), padding='same')
        self.act2A = Activation('relu')
        self.bn2A = BatchNormalization(axis=chanDim)
        self.conv2B = Conv2D(filters=32, kernel_size=(3, 3), padding='same')
        self.act2B = Activation('relu')
        self.bn2B = BatchNormalization(axis=chanDim)
        self.pool2 = MaxPool2D(pool_size=(2, 2))

        self.flatten = Flatten()
        self.dense3 = Dense(units=512)
        self.act3 = Activation('relu')
        self.bn3 = BatchNormalization(axis=chanDim)
        self.do3 = Dropout(0.5)

        self.dense4 = Dense(units=classes)
        self.softmax = Activation('softmax')

    def call(self, inputs):
        x = self.conv1A(inputs)
        x = self.act1A(x)
        x = self.bn1A(x)
        x = self.conv1B(x)
        x = self.act1B(x)
        x = self.bn1B(x)
        x = self.pool1(x)

        x = self.conv2A(x)
        x = self.act2A(x)
        x = self.bn2A(x)
        x = self.conv2B(x)
        x = self.act2B(x)
        x = self.bn2B(x)
        x = self.pool2(x)

        x = self.flatten(x)
        x = self.dense3(x)
        x = self.act3(x)
        x = self.bn3(x)
        x = self.do3(x)

        x = self.dense4(x)
        x = self.softmax(x)

        return x

