import tensorflow as tf
from tensorflow.keras.applications import mobilenet, imagenet_utils
from tensorflow.keras.preprocessing import image


import numpy as np


def prepare_image(file):
    img_path = 'data/MobileNet-samples/'
    img = image.load_img(img_path+file, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array_expanded_dim = np.expand_dims(img_array, axis=0)
    return tf.keras.applications.mobilenet.preprocess_input(img_array_expanded_dim)


mobile = mobilenet.MobileNet()
img_preprocessed = prepare_image('3.png')
predictions = mobile.predict(img_preprocessed)
results = imagenet_utils.decode_predictions(predictions)
for result in results[0]:
    print(result)


print('Done!')