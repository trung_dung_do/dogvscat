import matplotlib
matplotlib.use('Agg')

import logging
logging.getLogger('tensorflow').setLevel(logging.CRITICAL)

from tensorflow_model import shallownet_sequential
from tensorflow_model import minigooglenet_functional
from tensorflow_model import MiniVGGNetModel
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import classification_report
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
import argparse

ap = argparse.ArgumentParser()
ap.add_argument('-m', '--model', type=str, default='class',
                choices=['sequential', 'functional', 'class'],
                help='type of model architecture')

ap.add_argument('-p', '--plot', type=str, default='output/class.png',
                help='path to output plot file')

args = vars(ap.parse_args())

INIT_LR = 1e-2
BATCH_SIZE = 128
NUM_EPOCHS = 1

labelNames = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog',
              'frog', 'horse', 'ship', 'truck']

print('[INFO] loading cifar-10 dataset...')
((trainX, trainY), (testX, testY)) = cifar10.load_data()
trainX = trainX.astype('float32') / 255.0
testX = testX.astype('float32') / 255.0

lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.fit_transform(testY)

aug = ImageDataGenerator(rotation_range=18, zoom_range=0.15,
                         width_shift_range=0.2, height_shift_range=0.2,
                         shear_range=0.15, horizontal_flip=True,
                         fill_mode='nearest')

if args['model'] == 'sequential':
    print('[INFO] using sequential model...')
    model = shallownet_sequential(32, 32, 3, len(labelNames))
elif args['model'] == 'functional':
    print('[INFO] using functional model...')
    model = minigooglenet_functional(32, 32, 3, len(labelNames))
elif args['model'] == 'class':
    print('[INFO] using model sub-classing...')
    model = MiniVGGNetModel(len(labelNames))

model.build((1, 32, 32, 3))
model.summary()


opt = SGD(lr=INIT_LR, momentum=0.9, decay=INIT_LR / NUM_EPOCHS)
print('[INFO] training network...')
model.compile(loss='categorical_crossentropy', optimizer=opt,
              metrics=['accuracy'])


H = model.fit_generator(
    aug.flow(trainX, trainY, batch_size=BATCH_SIZE),
    validation_data=(testX, testY),
    steps_per_epoch=trainX.shape[0] // BATCH_SIZE,
    epochs=NUM_EPOCHS,
    verbose=1
)
#
# model.summary()



print('[INFO] evaluating network...')
predictions = model.predict(testX, batch_size=BATCH_SIZE)
print(classification_report(testY.argmax(axis=1),
                            predictions.argmax(axis=1), target_names=labelNames))

N = np.arange(0, NUM_EPOCHS)
title = 'Training loss and accuracy on cifar-10 ({})'.format(args['model'])

plt.style.use('ggplot')
plt.figure()
plt.plot(N, H.history['loss'], label='train_loss')
plt.plot(N, H.history['val_loss'], label='val_loss')
plt.plot(N, H.history['acc'], label='train_acc')
plt.plot(N, H.history['val_acc'], label='val_acc')
plt.title(title)
plt.xlabel('Epoch #')
plt.ylabel('Loss/Accuracy')
plt.legend()
plt.savefig(args['plot'])