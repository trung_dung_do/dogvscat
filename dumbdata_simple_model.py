import numpy as np
from random import randint
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import load_model, model_from_json


def train_model():
    train_labels = []
    train_samples = []

    for i in range(50):
        random_younger = randint(13, 64)
        train_samples.append(random_younger)
        train_labels.append(1)

        random_older = randint(65, 100)
        train_samples.append(random_older)
        train_labels.append(0)

    for i in range(1000):
        random_younger = randint(13, 64)
        train_samples.append(random_younger)
        train_labels.append(0)

        random_older = randint(65, 100)
        train_samples.append(random_older)
        train_labels.append(1)

    train_samples = np.array(train_samples)
    train_labels = np.array(train_labels)
    train_labels, train_samples = shuffle(train_labels, train_samples)

    scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_train_samples = scaler.fit_transform(train_samples.reshape(-1, 1))

    # create a deep learning model
    model = Sequential()
    model.add(Dense(units=16, input_shape=(1,), activation='relu'))
    model.add(Dense(units=32, activation='relu'))
    model.add(Dense(units=2, activation='softmax'))

    model.summary()

    # compile the model
    model.compile(optimizer=Adam(learning_rate=0.001),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    # train the model
    model.fit(x=scaled_train_samples, y=train_labels, validation_split=0.1, batch_size=10,
              epochs=30, shuffle=True, verbose=2)

    # model.save('model_full.h5')
    # save the model only
    model_json = model.to_json()
    json_file = open('model.json', 'w')
    json_file.write(model_json)
    json_file.close()
    #save the weights
    model.save_weights('model.h5')


def predict_model():
    # generating the test set
    test_samples = []
    test_labels = []
    for i in range(10):
        random_younger = randint(13, 64)
        test_samples.append(random_younger)
        test_labels.append(1)

        random_older = randint(65, 100)
        test_samples.append(random_older)
        test_labels.append(0)

    for i in range(200):
        random_younger = randint(13, 64)
        test_samples.append(random_younger)
        test_labels.append(0)

        random_older = randint(65, 100)
        test_samples.append(random_older)
        test_labels.append(1)

    test_samples = np.array(test_samples)
    test_labels = np.array(test_labels)
    test_samples, test_labels = shuffle(test_samples, test_labels)

    scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_test_samples = scaler.fit_transform(test_samples.reshape(-1, 1))

    # load model
    # model = load_model('model_full.h5')

    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)

    model.load_weights('model.h5')


    # predict
    predictions = model.predict(x=scaled_test_samples, batch_size=10, verbose=0)

    for i in predictions:
        print(i)

    predictions = np.argmax(predictions, axis=-1)
    for i in predictions:
        print(i)


# train_model()
predict_model()
print('Done!')









